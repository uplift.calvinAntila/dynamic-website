const goTop = document.querySelector('.go-top');
const navBar = document.querySelectorAll('.navbar-nav a');
const items = document.querySelectorAll('.items li');
const images = document.querySelectorAll('.image-container img');

//go to top button
window.addEventListener('scroll', () => {
    if (window.pageYOffset > 100) {
        goTop.classList.add('active-scroll');
    }
    else {
        goTop.classList.remove('active-scroll')
    }
})

//navbar selected
navBar.forEach(a => {
    a.addEventListener('click', () => {
        navBar.forEach(a => {
            a.classList.remove('active-nav');
        })
        a.classList.add('active-nav');
    })
})

// for product sorting
items.forEach(li => {
    li.addEventListener('click', () => {
        items.forEach(li => {
            li.classList.remove('active');
        })
        li.classList.add('active');

        const imageContent = li.textContent;
        images.forEach(img => {
            img.classList.add('hide-img');
            if (img.getAttribute('data-sort') === imageContent.toLowerCase() || imageContent === 'All') {
                img.classList.replace('hide-img', 'show-img');
            }
            img.classList.remove('show-img');
        })
    })
})